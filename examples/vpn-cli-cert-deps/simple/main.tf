provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "vpn_client_deps" {
  source          = "../../../modules/vpn-cli-cert-deps"
  globals         = module.common.globals
  KMS_KEY_ID      = module.encryption.kms_key_id
  LAMBDA_ROLE_ARN = "arn:aws:iam::737020658184:role/cpt-ops"
  USERS           = var.USERS
  ACCOUNT_ID      = data.aws_caller_identity.current.account_id
  LAMBDA_NAME     = "lambda"
}

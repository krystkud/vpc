variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "USERS" {
  description = "Users"
  type        = list(map(string))
}

data "aws_caller_identity" "current" {}

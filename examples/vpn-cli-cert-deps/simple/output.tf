# Locale variables with default tags values
locals {
  default_tags = {
    "Environment" = substr(random_string.env.id, 0, min(4, length(random_string.env.id)))
    "Project"     = substr(random_pet.pet.id, 0, min(20, length(random_pet.pet.id)))
  }
}

output "info" {
  value = <<OUTPUT

MANAGEMENT VPC:
- NAME                   :: ${local.default_tags["Project"]} - ${local.default_tags["Environment"]}
- REGION                 :: ${var.AWS_REGION}


OUTPUT

}


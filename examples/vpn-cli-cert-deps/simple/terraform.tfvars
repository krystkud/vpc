AWS_REGION   = "us-east-1"
ACCOUNT_NAME = "teribu"
USERS = [{
  name   = "Bartlomiej.Sawicki",
  sudo   = "ALL=(ALL) NOPASSWD:ALL",
  shell  = "/bin/bash"
  uid    = 1001
  groups = "docker"
  arn    = "arn:aws:sts::737020658184:assumed-role/cpt-admin/Bartlomiej.SAWICKI@idemia.com"
  },
  {
    name   = "Test.Test",
    sudo   = "ALL=(ALL) NOPASSWD:ALL",
    shell  = "/bin/bash"
    uid    = 1002
    groups = "docker"
    arn    = "arn:aws:sts::737020658184:assumed-role/cpt-admin/Test.Test@idemia.com"
  }
]

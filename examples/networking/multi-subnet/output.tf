output "VPC" {
  value = <<OUTPUT

SIMPLE VPC:
- ID                    :: ${module.networking.vpc_id}
- Availability Zones    :: ${join(", ", module.networking.application_availability_zones)}
- DOPT                  :: ${module.networking.dopt}

Subnets:
- Public                :: ${join(", ", module.networking.public_subnets)}
- Application           :: ${join(", ", module.networking.application_subnets)}
- Data                  :: ${join(", ", module.networking.data_subnets)}

NAT:
- Public IP             :: ${join(", ", module.networking.nat_public_ips)}
- Private IP            :: ${join(", ", module.networking.nat_private_ips)}


###################################################################################

OUTPUT

}

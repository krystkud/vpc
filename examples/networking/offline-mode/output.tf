output "VPC" {
  value = <<OUTPUT

SIMPLE VPC:
- ID                    :: ${module.networking.vpc_id}
- Availability Zones    :: ${join(", ", module.networking.application_availability_zones)}
- DOPT                  :: ${module.networking.dopt}

Subnets:
- Application           :: ${join(", ", module.networking.application_subnets)}

###################################################################################

OUTPUT

}

AWS_REGION              = "us-west-2"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.10.0.0/23"
ADDITIONAL_CIDRS        = ["10.11.0.0/23", "10.12.0.0/23", "10.13.0.0/23"]
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.10.1.0/24", "10.11.0.0/24", "10.12.0.0/24", "10.13.0.0/24"]

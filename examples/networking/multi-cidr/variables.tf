variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "ADDITIONAL_CIDRS" {
  default     = []
  type        = list(string)
  description = "List of Additional CIDR Blocks for the VPC. Must be compatible with main CIDR, see https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html#vpc-resize"
}

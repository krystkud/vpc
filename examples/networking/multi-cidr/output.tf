output "VPC" {
  value = <<OUTPUT

SIMPLE VPC:
- ID                    :: ${module.networking.vpc_id}
- Availability Zones    :: ${join(", ", module.networking.application_availability_zones)}
- DOPT                  :: ${module.networking.dopt}
Subnets:
- Public                :: ${join(", ", module.networking.public_subnets)}
- Application           :: ${join(", ", module.networking.application_subnets)}

CIDR :: ${var.VPC_CIDR_BLOCK} - ${join(" - ", var.ADDITIONAL_CIDRS)}
###################################################################################

OUTPUT

}

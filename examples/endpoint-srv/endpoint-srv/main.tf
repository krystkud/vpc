provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source          = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME    = var.ACCOUNT_NAME
  AWS_REGION      = var.AWS_REGION
  ENVIRONMENT     = random_string.env.id
  PROJECT         = random_pet.pet.id
  PUBLIC_KEY_PATH = var.PUBLIC_KEY_PATH
}

module "encryption" {
  source              = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals             = module.common.globals
  KMS_KEY_NAME_SUFFIX = "srv"
}

module "networking" {
  source                  = "../../../modules/networking"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
}

module "ec2_node_group_a" {
  source                = "git::https://bitbucket.org/digitallabsbuild/ec2.git//modules/node_group?ref=master"
  globals               = module.common.globals
  networking            = module.networking.common
  KMS_KEY_ID            = module.encryption.kms_key_id
  SUBNET_IDS            = module.networking.application_subnet_ids
  CUSTOM_USER_DATA      = var.USERDATA
  TARGET_GROUP_ARNS     = [module.ingress_nlb.ingress_80_arn]
  CUSTOM_INGRESS_ACCESS = ["0.0.0.0/0:80"]
  NODE_GROUP_SIZE       = 1
}

module "ingress_nlb" {
  source                = "git::https://bitbucket.org/digitallabsbuild/ingress.git//modules/nlb/?ref=master"
  globals               = module.common.globals
  networking            = module.networking.common
  PUBLIC_SUBNET_IDS     = module.networking.application_subnet_ids
  INTERNAL              = true
  BACKEND_PORT          = 80
  HEALTH_CHECK_PORT     = 80
  HEALTH_CHECK_URL      = "/"
  ENABLE_HTTPS_LISTENER = false
}

module "endpoint_service" {
  source                 = "../../../modules/endpoint-srv"
  globals                = module.common.globals
  NETWORK_LOAD_BALANCERS = [module.ingress_nlb.nlb_arn]
}

provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

data "terraform_remote_state" "vpc1" {
  backend = "local"

  config = {
    path = "../endpoint-srv/terraform.tfstate"
  }
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source              = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals             = module.common.globals
  KMS_KEY_NAME_SUFFIX = "cli"
}

module "networking" {
  source                  = "../../../modules/networking"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
  USE_NAT_GATEWAY         = false
}

module "service_endpoint" {
  source              = "../../../modules/endpoint"
  globals             = module.common.globals
  networking          = module.networking.common
  ENDPOINT_TYPE       = "Interface"
  SERVICE_NAME        = data.terraform_remote_state.vpc1.outputs.endpoint_name
  SUBNET_IDS          = module.networking.public_subnet_ids
  INGRESS_PORTS       = [80]
  AUTO_ACCEPT         = true
  ENABLED_PRIVATE_DNS = false
  DNS_NAME            = "coolendpoint"
}

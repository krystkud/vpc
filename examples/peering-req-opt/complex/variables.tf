variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  default     = "teribu"
  type        = string
}

variable "VPCA_VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "172.16.0.0/23"
  type        = string
}

variable "VPCA_PUBLIC_CIDR_BLOCKS" {
  default     = ["172.16.0.0/26"]
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "VPCA_APPLICATION_CIDR_BLOCKS" {
  default     = ["172.16.1.0/26"]
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "VPCB_VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "172.20.0.0/23"
  type        = string
}

variable "VPCB_PUBLIC_CIDR_BLOCKS" {
  default     = ["172.20.0.0/26"]
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "VPCB_APPLICATION_CIDR_BLOCKS" {
  default     = ["172.16.1.0/26"]
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "VPCB_REGION" {
  default     = "us-east-2"
  description = "Provide VPC B region"
  type        = string
}

variable "ENVIRONMENT_A" {
  default     = "vpca"
  description = "Provide env a name"
  type        = string
}

variable "ENVIRONMENT_B" {
  default     = "vpcb"
  description = "Provide env b name"
  type        = string
}

variable "PEERING_REQ_NAME" {
  description = "Name of peering request"
  type        = string
  default     = ""
}

variable "PEERING_ACC_NAME" {
  description = "Name of peering request"
  type        = string
  default     = ""
}
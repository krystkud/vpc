provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "mgmt" {
}

resource "random_pet" "proj" {
}

module "mgmt_common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = "eu-west-1"
  ENVIRONMENT  = "mgmt"
  PROJECT      = random_pet.mgmt.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.mgmt_common.globals
}

module "mgmt_networking" {
  source                  = "../../../modules/networking"
  globals                 = module.mgmt_common.globals
  VPC_CIDR_BLOCK          = var.MGMT_VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.MGMT_PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.MGMT_APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
}

##
# Define TGW Association with Project VPC
##
module "transit" {
  source     = "../../../modules/transit-gateway"
  globals    = module.mgmt_common.globals
  networking = module.mgmt_networking.common
  SUBNET_IDS = module.mgmt_networking.public_subnet_ids
}

module "project_common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = "proj"
  PROJECT      = random_pet.proj.id
}

module "project_networking" {
  source                  = "../../../modules/networking"
  globals                 = module.project_common.globals
  VPC_CIDR_BLOCK          = var.PROJECT_VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PROJECT_PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.PROJECT_APPLICATION_CIDR_BLOCKS
}

module "attachment" {
  source             = "../../../modules/transit-attach"
  globals            = module.project_common.globals
  networking         = module.project_networking.common
  SUBNET_IDS         = module.project_networking.application_subnet_ids
  TRANSIT_GATEWAY_ID = module.transit.transit_gateway_id
}

module "association_routes" {
  source = "../../../modules/transit-routes"
  ROUTE_TABLE_IDS = concat(
    module.project_networking.public_route_table_ids,
    module.project_networking.application_route_table_ids,
  )
  DESTINATION_CIDR_BLOCK = var.MGMT_VPC_CIDR_BLOCK
  TRANSIT_GATEWAY_ID     = module.transit.transit_gateway_id
}

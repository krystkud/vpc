provider "aws" {
  region = var.AWS_REGION
}

provider "aws" {
  alias  = "vpcb"
  region = var.VPCB_REGION
}

resource "random_pet" "pet" {
}

module "vpca_common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT_A
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.vpca_common.globals
}

module "vpca_networking" {
  source                  = "../../../modules/networking"
  globals                 = module.vpca_common.globals
  VPC_CIDR_BLOCK          = var.VPCA_VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.VPCA_PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.VPCA_APPLICATION_CIDR_BLOCKS
  USE_NAT_GATEWAY         = true
}

module "vpcb_common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.VPCB_REGION
  ENVIRONMENT  = var.ENVIRONMENT_B
  PROJECT      = random_pet.pet.id
}

module "vpcb_networking" {
  source                  = "../../../modules/networking"
  globals                 = module.vpcb_common.globals
  VPC_CIDR_BLOCK          = var.VPCB_VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.VPCB_PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.VPCB_APPLICATION_CIDR_BLOCKS

  providers = {
    aws = aws.vpcb
  }
}

module "vpc_peering_with_vpcb" {
  source           = "../../../modules/peering-req"
  globals          = module.vpca_common.globals
  networking       = module.vpca_networking.common
  PEER_VPC_ID      = module.vpcb_networking.vpc_id
  PEER_REGION      = var.VPCB_REGION
  PEERING_REQ_NAME = var.PEERING_REQ_NAME
}

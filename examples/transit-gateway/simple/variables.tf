variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-west-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  default     = "teribu"
  type        = string
}

variable "MGMT_VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "172.20.0.0/23"
  type        = string
}

variable "MGMT_PUBLIC_CIDR_BLOCKS" {
  default     = ["172.20.0.0/26"]
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "MGMT_APPLICATION_CIDR_BLOCKS" {
  default     = ["172.20.1.0/26"]
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "PROJECT_VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "192.168.0.0/24"
  type        = string
}

variable "PROJECT_PUBLIC_CIDR_BLOCKS" {
  default     = ["192.168.0.0/27"]
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "PROJECT_APPLICATION_CIDR_BLOCKS" {
  default     = ["192.168.0.96/27"]
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

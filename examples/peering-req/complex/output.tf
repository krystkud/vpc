output "vpca" {
  value = <<OUTPUT

VPC A:
- ID                    :: ${module.vpca_networking.vpc_id}
- Availability Zones    :: ${join(", ", module.vpca_networking.application_availability_zones)}
- CIDR                  :: ${var.VPCA_VPC_CIDR_BLOCK}

Subnets:
- Public                :: ${join(", ", module.vpca_networking.public_subnets)}
- Application           :: ${join(", ", module.vpca_networking.application_subnets)}

Peering:
- Peering ID            :: ${module.vpc_peering_with_vpcb.vpc_peering_connection_id}


###################################################################################

OUTPUT

}

output "vpcb" {
  value = <<OUTPUT

VPC B:
- ID                    :: ${module.vpcb_networking.vpc_id}
- Availability Zones    :: ${join(", ", module.vpcb_networking.application_availability_zones)}
- CIDR                  :: ${var.VPCB_VPC_CIDR_BLOCK}

Subnets:
- Public                :: ${join(", ", module.vpcb_networking.public_subnets)}
- Application           :: ${join(", ", module.vpcb_networking.application_subnets)}

Peering:
- Status                :: ${module.vpc_peering_with_vpcb.vpc_peering_connection_accept_status}
- Peering Name          :: ${var.PEERING_ACC_NAME}

###################################################################################

OUTPUT

}

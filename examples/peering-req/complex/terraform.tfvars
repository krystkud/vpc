AWS_REGION   = "eu-west-1"
ACCOUNT_NAME = "teribu"

ENVIRONMENT_A                = "vpca"
VPCA_VPC_CIDR_BLOCK          = "172.16.0.0/23"
VPCA_PUBLIC_CIDR_BLOCKS      = ["172.16.0.0/26"]
VPCA_APPLICATION_CIDR_BLOCKS = ["172.16.1.0/26"]
PEERING_REQ_NAME             = "peering-to-vpc-b"

ENVIRONMENT_B                = "vpcb"
VPCB_REGION                  = "us-east-2"
VPCB_VPC_CIDR_BLOCK          = "172.20.0.0/23"
VPCB_PUBLIC_CIDR_BLOCKS      = ["172.20.0.0/26"]
VPCB_APPLICATION_CIDR_BLOCKS = ["172.20.1.0/26"]
PEERING_ACC_NAME             = "peering-to-vpc-a"
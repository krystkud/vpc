AWS_REGION              = "eu-west-3"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.20.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.20.0.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.20.10.0/24", "10.20.11.0/24", "10.20.12.0/24"]

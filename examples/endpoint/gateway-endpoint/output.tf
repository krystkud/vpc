# Locale variables with default tags values
locals {
  default_tags = {
    "Environment" = substr(random_string.env.id, 0, min(4, length(random_string.env.id)))
    "Project"     = substr(random_pet.pet.id, 0, min(20, length(random_pet.pet.id)))
  }
}

output "info" {
  value = <<OUTPUT

MANAGEMENT VPC:
- NAME                   :: ${local.default_tags["Project"]} - ${local.default_tags["Environment"]}
- ID                     :: ${module.networking.vpc_id}
- REGION                 :: ${var.AWS_REGION}
- Availability Zones     :: ${join(", ", module.networking.application_availability_zones)}

Subnets:
- Public                 :: ${join(", ", module.networking.public_subnets)}
- Application            :: ${join(", ", module.networking.application_subnets)}
- Data                   :: ${join(", ", module.networking.data_subnets)}

S3 Endpoint ID           :: ${join(", ", module.s3_endpoint.gateway_id)}
S3 Endpoint Status       :: ${join(", ", module.s3_endpoint.gateway_state)}

DynamoDB Endpoint ID     :: ${join(", ", module.dynamo_db_endpoint_with_policy.gateway_id)}
DynamoDB Endpoint Status :: ${join(", ", module.dynamo_db_endpoint_with_policy.gateway_state)}

OUTPUT

}

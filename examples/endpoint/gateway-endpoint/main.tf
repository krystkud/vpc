provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = random_string.env.id
  PROJECT      = random_pet.pet.id
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "networking" {
  source                  = "../../../modules/networking"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
}

module "s3_endpoint" {
  source          = "../../../modules/endpoint"
  globals         = module.common.globals
  networking      = module.networking.common
  SERVICE_NAME    = "com.amazonaws.${var.AWS_REGION}.s3"
  ROUTE_TABLE_IDS = module.networking.application_route_table_ids
}

data "aws_iam_policy_document" "dynamo_db_vpc_endpoint_policy" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:*",
    ]
    resources = [
      "*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

module "dynamo_db_endpoint_with_policy" {
  source                  = "../../../modules/endpoint"
  globals                 = module.common.globals
  networking              = module.networking.common
  SERVICE_NAME            = "com.amazonaws.${var.AWS_REGION}.dynamodb"
  ROUTE_TABLE_IDS         = module.networking.application_route_table_ids
  GATEWAY_ENDPOINT_POLICY = data.aws_iam_policy_document.dynamo_db_vpc_endpoint_policy.json
}

data "aws_caller_identity" "current" {}

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "PUBLIC_KEY_PATH" {
  description = "Provide path to ssh public key"
  type        = string
}

variable "AMI_OWNER" {
  default     = ""
  description = "Provide ami owner"
  type        = string
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "Provide additional ssh public keys"
  default     = []
  type        = list(string)
}

variable "USERS" {
  description = "Users"
  type        = list(map(string))
}

provider "aws" {
  region = var.AWS_REGION
}

resource "random_pet" "pet" {
}

resource "random_string" "env" {
  length  = 4
  upper   = false
  number  = false
  special = false
}

module "common" {
  source          = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=master"
  ACCOUNT_NAME    = var.ACCOUNT_NAME
  AWS_REGION      = var.AWS_REGION
  ENVIRONMENT     = random_string.env.id
  PROJECT         = random_pet.pet.id
  PUBLIC_KEY_PATH = var.PUBLIC_KEY_PATH
  AMI_OWNER       = var.AMI_OWNER
}

module "encryption" {
  source  = "git::https://bitbucket.org/digitallabsbuild/encryption.git//modules?ref=master"
  globals = module.common.globals
}

module "networking" {
  source                  = "../../../modules/networking"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
}

module "bastion" {
  source                = "git::https://bitbucket.org/digitallabsbuild/bastion.git//modules?ref=master"
  globals               = module.common.globals
  networking            = module.networking.common
  SUBNET_IDS            = module.networking.application_subnet_ids
  KMS_KEY_ID            = module.encryption.kms_key_id
  ADDITIONAL_SSH_KEYS   = var.ADDITIONAL_SSH_KEYS
  BASTION_ACCESS_CIDR   = ["0.0.0.0/0"]
  ENABLE_SPOT_INSTANCES = true
  INTERNAL_BASTION      = true
}

#Invocation of Lambda should be splited to different terraform run, certificate arn should be known before run
#if not, terraform will recreate VPN endpoint.

module "cert_lambda" {
  source = "git::https://bitbucket.org/digitallabsbuild/lambda.git//modules/lambda?ref=master"

  HANDLER           = "lambda_function.lambda_handler"
  NAME              = random_pet.pet.id
  FILENAME          = "lambda_function.py"
  LAMBDA_PATH       = "/"
  LAMBDA_GIT        = "ssh://git@bitbucket.oberthur.com/tmau/lambda_functions.git"
  LAMBDA_GIT_BRANCH = "0.25.0"
  RUNTIME           = "python3.7"
  TIMEOUT           = 360
  MEMORY_SIZE       = 1500
  globals           = module.common.globals
  ENVIRONMENT       = { variables = { "ENV_NAME" : module.common.globals["Environment"], "PROJECT" : module.common.globals["Project"], "USERS" : join(" ", var.USERS.*.name), "FORCE_NEW_CA" : "True" } }
  IAM_PERMISSIONS = [{
    IAM_ACTIONS = [
      "logs:PutLogEvents",
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "acm:ImportCertificate",
      "acm:AddTagsToCertificate",
      "secretsmanager:CreateSecret",
      "secretsmanager:TagResource",
      "secretsmanager:GetSecretValue",
      "secretsmanager:PutSecretValue",
      "acm:ListCertificates",
      "acm:ListTagsForCertificate",
      "acm:DescribeCertificate",
      "kms:GenerateDataKey",
      "kms:Decrypt"
    ],
    IAM_RESOURCE = ["*"] }
  ]
  PIP_INSTALL                  = "cffi cryptography jmespath"
  PIP_INSTALL_WITH_AMAZONLINUX = true
}

# This step can failed with permission denied if Lambda is not yet ready (rerun is successful)
data "aws_lambda_invocation" "example" {
  function_name = module.cert_lambda.function_name

  input      = <<JSON
{
}
JSON
  depends_on = [module.cert_lambda, module.vpn_client_deps]
}

module "vpn_client_deps" {
  source          = "../../../modules/vpn-cli-cert-deps"
  globals         = module.common.globals
  KMS_KEY_ID      = module.encryption.kms_key_id
  LAMBDA_ROLE_ARN = module.cert_lambda.function_role_arn
  LAMBDA_NAME     = module.cert_lambda.function_name
  ACCOUNT_ID      = data.aws_caller_identity.current.account_id
  USERS           = var.USERS
}

module "vpn_client" {
  source = "../../../modules/vpn-cli"

  ASSOSIATED_SUBNET_IDS  = module.networking.application_subnet_ids
  SERVER_CERTIFICATE_ARN = jsondecode(data.aws_lambda_invocation.example.result)["body"]["server_certificate_arn"]
  globals                = module.common.globals
  CLIENT_CERTIFICATE_ARN = jsondecode(data.aws_lambda_invocation.example.result)["body"]["server_certificate_arn"]
  INGRESS_CIDR           = var.VPC_CIDR_BLOCK
  VPC_ID                 = module.networking.vpc_id
  CRL_SECRET             = module.vpn_client_deps.crl_secret_name #tfsec:ignore:general-secrets-sensitive-in-attribute
}

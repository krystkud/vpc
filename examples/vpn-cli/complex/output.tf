# Locale variables with default tags values
locals {
  default_tags = {
    "Environment" = substr(random_string.env.id, 0, min(4, length(random_string.env.id)))
    "Project"     = substr(random_pet.pet.id, 0, min(20, length(random_pet.pet.id)))
  }
}

output "info" {
  value = <<OUTPUT

MANAGEMENT VPC:
- NAME                   :: ${local.default_tags["Project"]} - ${local.default_tags["Environment"]}
- ID                     :: ${module.networking.vpc_id}
- REGION                 :: ${var.AWS_REGION}
- Availability Zones     :: ${join(", ", module.networking.application_availability_zones)}

Subnets:
- Public                 :: ${join(", ", module.networking.public_subnets)}
- Application            :: ${join(", ", module.networking.application_subnets)}
- Data                   :: ${join(", ", module.networking.data_subnets)}



OUTPUT

}


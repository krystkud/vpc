AWS_REGION              = "eu-west-1"
KMS_KEY_NAME_SUFFIX     = "example-vpn-client"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.20.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.20.0.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.20.10.0/24", "10.20.11.0/24", "10.20.12.0/24"]
PUBLIC_KEY_PATH         = "ubuntu-key.pub"
AMI_OWNER               = "737020658184"
ADDITIONAL_SSH_KEYS = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDhZfWJvZ07vMyYk5R1oo/T11JYlg6KyU5RKxGCOzSpWi5hgzTmZ4r7bPcMqJMm9kAUOmdqsDiIEZPbmRJgPM+Jr2dkJ7rdG87OQoaOYO3J5DtQfaolTCv37wFg2UWYlI6PRvgmMZ/uy8Kx83Shdj9xV8KQuS1R8cRo/CeQ/KTtg2eJwmfMGxOK81lER9KHyECRJZO0n7GeTN9jqG1YgdUnc/w2+SDqBKoZfL1ZAsSlep+wmhWOJTydzOk1wXrwlCrnrI0q2CFhhmpRTBLHN5MJwFQjxIlj/yb1mnwdMoAzFFfE291RnBKljAq7WY9OI3BgBK2s+d7y3rhT/A48KGsl", #paweljez
]
BASTION_ACCESS_CIDR     = ["10.10.0.0/16", "194.145.235.0/24", "185.130.180.0/22", "188.164.241.90/32"]
DESTINATION_CIDR_BLOCKS = ["10.10.0.0/16"]

USERS = [{
  name   = "Bartlomiej.Sawicki",
  sudo   = "ALL=(ALL) NOPASSWD:ALL",
  shell  = "/bin/bash"
  uid    = 1001
  groups = "docker"
  arn    = "arn:aws:sts::737020658184:assumed-role/cpt-admin/Bartlomiej.SAWICKI@idemia.com"
  },
  {
    name   = "Test.Test",
    sudo   = "ALL=(ALL) NOPASSWD:ALL",
    shell  = "/bin/bash"
    uid    = 1002
    groups = "docker"
    arn    = "arn:aws:sts::737020658184:assumed-role/cpt-admin/Test.Test@idemia.com"
  }
]

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "VPN_GW_NAME_SUFFIX" {
  default     = "test"
  type        = string
  description = "VPN gateway suffix"
}

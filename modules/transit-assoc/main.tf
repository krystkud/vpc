##
# Define transit gateway route table for main
##
resource "aws_ec2_transit_gateway_route_table" "main" {
  transit_gateway_id = var.TRANSIT_GATEWAY_ID

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-to-all",
      "Object", "aws_ec2_transit_gateway_route_table"
    )
  )
}

##
# Define transit gateway route table for attachment
##
resource "aws_ec2_transit_gateway_route_table" "attachment" {
  count              = length(var.ASSOCIATED_VPC_IDS)
  transit_gateway_id = var.TRANSIT_GATEWAY_ID

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-from-${var.ASSOCIATED_VPC_IDS[count.index]}",
      "Object", "aws_ec2_transit_gateway_route_table"
    )
  )
}

##
# Associate transit gateway attachment with vpc route table
##
resource "aws_ec2_transit_gateway_route_table_association" "main" {
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.main.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.main.id
}

##
# Associate transit gateway attachment with vpc route table
##
resource "aws_ec2_transit_gateway_route_table_association" "attachment" {
  count                          = length(var.ASSOCIATED_VPC_IDS)
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.associated_vpc[count.index].id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.attachment[count.index].id
}

##
# Propagate transit gateway route table with provided vpc
##
resource "aws_ec2_transit_gateway_route_table_propagation" "main" {
  count                          = length(var.ASSOCIATED_VPC_IDS)
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.associated_vpc[count.index].id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.main.id
}

##
# Propagate transit gateway route table with provided vpc
##
resource "aws_ec2_transit_gateway_route_table_propagation" "attachment" {
  count                          = length(var.ASSOCIATED_VPC_IDS)
  transit_gateway_attachment_id  = data.aws_ec2_transit_gateway_vpc_attachment.main.id
  transit_gateway_route_table_id = aws_ec2_transit_gateway_route_table.attachment[count.index].id
}

variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-transit-assoc-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

##
# Get aws_ec2_transit_gateway_vpc_attachment for current vpc
##
data "aws_ec2_transit_gateway_vpc_attachment" "main" {
  filter {
    name   = "transit-gateway-id"
    values = [var.TRANSIT_GATEWAY_ID]
  }

  filter {
    name   = "vpc-id"
    values = [var.networking["VpcId"]]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

##
# Get aws_ec2_transit_gateway_vpc_attachment for associated vpc
##
data "aws_ec2_transit_gateway_vpc_attachment" "associated_vpc" {
  count = length(var.ASSOCIATED_VPC_IDS)

  filter {
    name   = "transit-gateway-id"
    values = [var.TRANSIT_GATEWAY_ID]
  }

  filter {
    name   = "vpc-id"
    values = [var.ASSOCIATED_VPC_IDS[count.index]]
  }

  filter {
    name   = "state"
    values = ["available", "pending"]
  }
}

variable "ASSOCIATED_VPC_IDS" {
  default     = []
  type        = list(string)
  description = "Provide associated vpc ids"
}

variable "TRANSIT_GATEWAY_ID" {
  description = "Provide id of the transit gateway"
  type        = string
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module"
  type        = map(any)
}

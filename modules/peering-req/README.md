# Autogenerated content

## Dependencies

| Name | Versions |
|:------:|:----------:|
| terraform | >= 0.12.0 |
| aws | {'source': 'hashicorp/aws', 'version': '>=2.0'} |

## Used resources

| Type | Name |
|:------:|:------:|
| AWS | aws_route |
| AWS | aws_vpc_peering_connection |
| AWS | aws_vpc_peering_connection_options |

## Variables

| Variable | Default | Description | Type | Required |
|:----------:|:---------:|:-------------:|:------:|:----------:|
| AUTO_ACCEPT | False | Set to true to accept the peering automatically | ${bool} | False |
| MODULE_VERSION | 2.1.0 | Module version, update this value on commit to generate changelog and release | ${string} | False |
| PEERING_REQ_NAME |  | Name of peering request | ${string} | False |
| PEER_REGION |  | Provide peer AWS account region. | ${string} | True |
| PEER_VPC_ID |  | Provide associated vpc id | ${string} | True |
| PEER_VPC_OWNER_ID |  | Provide aws accounts id of the peered VPC | ${string} | False |
| globals |  | Provide global variables from common module. | ${map(any)} | True |
| networking |  | Provide common networking variables for other module. | ${map(any)} | True |

## Outputs

| Output | Description |
|:--------:|:-------------:|
| vpc_peering_connection_accept_status | Return VPC peering connection accept status. |
| vpc_peering_connection_id | Return VPC peering connection id. |

## Examples of potential use of this module:

 - complex

## Graphical representation

![graph](./modules/peering-req/graph.svg)

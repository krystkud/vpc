output "vpc_peering_connection_id" {
  value       = aws_vpc_peering_connection.peer_connection[0].id
  description = "Return VPC peering connection id."
}

output "vpc_peering_connection_accept_status" {
  value       = aws_vpc_peering_connection.peer_connection[0].accept_status
  description = "Return VPC peering connection accept status."
}
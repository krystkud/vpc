##
# Define vpc peering connection - Requester side
##
resource "aws_vpc_peering_connection" "peer_connection" {
  count         = var.PEER_VPC_ID == true ? 0 : 1
  vpc_id        = var.networking["VpcId"]
  peer_vpc_id   = var.PEER_VPC_ID
  peer_owner_id = var.PEER_VPC_OWNER_ID
  peer_region   = var.PEER_REGION
  auto_accept   = var.AUTO_ACCEPT

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.peering_req_name}-vpc-peering",
      "Object", "aws_vpc_peering",
      "Side", "Requester"
    )
  )
}

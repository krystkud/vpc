variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-peering-req-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
  peering_req_name = var.PEERING_REQ_NAME == "" ? local.environment_name : var.PEERING_REQ_NAME
}

variable "globals" {
  description = "Provide global variables from common module."
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module."
  type        = map(any)
}

variable "PEER_VPC_OWNER_ID" {
  default     = ""
  description = "Provide aws accounts id of the peered VPC"
  type        = string
}

variable "PEER_REGION" {
  description = "Provide peer AWS account region."
  type        = string
}

variable "PEER_VPC_ID" {
  description = "Provide associated vpc id"
  type        = string
}

variable "AUTO_ACCEPT" {
  default     = false
  type        = bool
  description = "Set to true to accept the peering automatically"
}
# tflint-ignore: terraform_unused_declarations
variable "PEERING_REQ_NAME" {
  description = "Name of peering request"
  type        = string
  default     = ""
}
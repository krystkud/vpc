output "client_vpn_endpoint_id" {
  value       = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  description = "ID of vpn endpoitn"
}

output "client_cidr_block" {
  value       = aws_ec2_client_vpn_endpoint.vpn_endpoint.client_cidr_block
  description = "CIDR of addresses assigned to users which connect to VPN"
}

output "log_group_name" {
  value       = aws_cloudwatch_log_group.vpn_log_group.name
  description = "Name of log group"
}

output "log_stream_name" {
  value       = aws_cloudwatch_log_stream.vpn_log_stream.name
  description = "Name of stream for logs"
}

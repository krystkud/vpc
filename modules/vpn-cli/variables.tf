variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-vpn-cli-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "CLIENT_CIDR_BLOCK" {
  description = "The IPv4 address range, in CIDR notation, from which to assign client IP addresses. The address range cannot overlap with the local CIDR of the VPC in which the associated subnet is located, or the routes that you add manually. The address range cannot be changed after the Client VPN endpoint has been created. The CIDR block should be /22 or greater."
  default     = "192.168.208.0/22"
  type        = string
}

variable "SPLIT_TUNNEL_ENABLED" {
  description = "Set split tunnel option"
  type        = bool
  default     = true
}

variable "ASSOSIATED_SUBNET_IDS" {
  description = "Provide list of subnet ids which should be available via vpn, can't be modified after apply because of bug https://github.com/terraform-providers/terraform-provider-aws/issues/7597"
  type        = list(string)
}

variable "SUFFIX" {
  description = "Suffix for name"
  type        = string
  default     = ""
}

variable "LOG_RETENTION" {
  description = "Log retention"
  default     = 365
  type        = number
}

variable "SERVER_CERTIFICATE_ARN" {
  description = "Server certificate arn from ACM"
  type        = string
}

variable "CLIENT_CERTIFICATE_ARN" {
  description = "Client certificate arn from ACM"
  type        = string
}

variable "INGRESS_CIDR" {
  description = "CIDR to which users should have access"
  type        = string
}

variable "VPC_ID" {
  description = "VPC id"
  type        = string
}

variable "CRL_SECRET" {
  description = "Secret name containing CRL for CA"
  type        = string
}

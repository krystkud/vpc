##
# Define customer gateway
##

#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "vpn_log_group" {
  name              = "${local.environment_name}-vpnc-${var.SUFFIX}"
  retention_in_days = var.LOG_RETENTION
}

resource "aws_cloudwatch_log_stream" "vpn_log_stream" {
  name           = "${local.environment_name}-vpnc-${var.SUFFIX}"
  log_group_name = aws_cloudwatch_log_group.vpn_log_group.name
}

resource "aws_ec2_client_vpn_endpoint" "vpn_endpoint" {
  description            = "terraform-clientvpn-example"
  server_certificate_arn = var.SERVER_CERTIFICATE_ARN
  client_cidr_block      = var.CLIENT_CIDR_BLOCK
  split_tunnel           = var.SPLIT_TUNNEL_ENABLED

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = var.CLIENT_CERTIFICATE_ARN
  }

  connection_log_options {
    enabled               = true
    cloudwatch_log_group  = aws_cloudwatch_log_group.vpn_log_group.name
    cloudwatch_log_stream = aws_cloudwatch_log_stream.vpn_log_stream.name
  }
  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${var.SUFFIX}",
      "Object", "aws_ec2_client_vpn_endpoint"
    )
  )
}

resource "aws_ec2_client_vpn_network_association" "subnets" {
  count = length(var.ASSOSIATED_SUBNET_IDS)
  #for_each = toset(var.ASSOSIATED_SUBNET_IDS)
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
  subnet_id              = sort(var.ASSOSIATED_SUBNET_IDS)[count.index]
  #Needed because of bug https://github.com/terraform-providers/terraform-provider-aws/issues/7597
  lifecycle {
    ignore_changes = [
      security_groups,
      subnet_id
    ]
  }
}

resource "null_resource" "ingress_authorization" {
  triggers = {
    run_on_recreate = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
    ingress         = var.INGRESS_CIDR
  }
  provisioner "local-exec" {
    command = "aws --region ${var.globals["Region"]} ec2 authorize-client-vpn-ingress --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn_endpoint.id} --target-network-cidr ${var.INGRESS_CIDR} --authorize-all-groups"
  }
}

resource "aws_security_group" "allow_vpn" {
  name        = "allow vpn outbound"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.VPC_ID

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.INGRESS_CIDR]
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-${var.SUFFIX}",
      "Object", "vpn secuirty group"
    )
  )
}

resource "null_resource" "attach_security_group" {
  triggers = {
    run_on_recreate = aws_ec2_client_vpn_endpoint.vpn_endpoint.id
    security_group  = aws_security_group.allow_vpn.id

  }
  provisioner "local-exec" {

    command = "aws --region ${var.globals["Region"]} ec2 apply-security-groups-to-client-vpn-target-network --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn_endpoint.id} --vpc-id ${var.VPC_ID}  --security-group-ids ${aws_security_group.allow_vpn.id}"
  }
}

resource "null_resource" "crl" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command    = "aws --region ${var.globals["Region"]} secretsmanager get-secret-value --secret-id ${var.CRL_SECRET} --query SecretString --output text > crl.pem"
    on_failure = continue
  }
}

resource "null_resource" "attach_crl" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = "if test $(wc -l crl.pem | cut -d ' ' -f 1) -gt 0; then aws --region ${var.globals["Region"]} ec2 import-client-vpn-client-certificate-revocation-list --client-vpn-endpoint-id ${aws_ec2_client_vpn_endpoint.vpn_endpoint.id} --certificate-revocation-list file://crl.pem; fi"

  }
  depends_on = [null_resource.crl]
}

output "service_name" {
  description = "Return service endpoint name"
  value       = aws_vpc_endpoint_service.nlb.service_name
}

output "service_type" {
  description = "Return service endpoint type"
  value       = aws_vpc_endpoint_service.nlb.service_type
}

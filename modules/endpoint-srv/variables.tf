variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-endpoint-srv-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}


variable "ACCEPTANCE_REQUIRED" {
  default     = true
  description = "Provide whether endpoint connection requests must be accepted"
  type        = bool
}

variable "NETWORK_LOAD_BALANCERS" {
  description = "Provide list of network load balancer arns"
  type        = list(string)
}

variable "ALLOWED_PRINCIPALS" {
  default     = []
  description = "Provide list of whitelisted aws accounts arns"
  type        = list(string)
}

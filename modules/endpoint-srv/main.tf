##
# Define vpc endpoint services
##
resource "aws_vpc_endpoint_service" "nlb" {
  acceptance_required        = var.ACCEPTANCE_REQUIRED
  network_load_balancer_arns = var.NETWORK_LOAD_BALANCERS
  allowed_principals         = var.ALLOWED_PRINCIPALS

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint-service",
      "Object", "aws_vpn_connection"
    )
  )
}

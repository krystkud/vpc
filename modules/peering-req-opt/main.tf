##
# Define vpc peering DNS resolution options
##
resource "aws_vpc_peering_connection_options" "peer_connection_options" {
  count                     = var.PEER_VPC_ID == true ? 0 : 1
  vpc_peering_connection_id = var.VPC_PEERING_ID
  requester {
    allow_remote_vpc_dns_resolution = var.REMOTE_VPC_DNS_RESOLUTION
  }
}

##
# Define routes to peered VPC
##
resource "aws_route" "peer_vpc_cidr_propagation" {
  count                     = length(var.ROUTE_TABLE_IDS)
  route_table_id            = var.ROUTE_TABLE_IDS[count.index]
  destination_cidr_block    = var.PEER_VPC_CIDR
  vpc_peering_connection_id = var.VPC_PEERING_ID
}

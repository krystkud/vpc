variable "PEER_VPC_ID" {
  description = "Provide associated vpc id"
  type        = string
}

variable "ROUTE_TABLE_IDS" {
  type        = list(string)
  description = "Provide list of route tables on which peer CIDR should be propagated."
}

variable "PEER_VPC_CIDR" {
  description = "CIDR of the peered VPC"
  type        = string
}

variable "REMOTE_VPC_DNS_RESOLUTION" {
  default     = true
  description = "Set it to true only after peering is Active"
  type        = bool
}

variable "VPC_PEERING_ID" {
  description = "Provide the VPC peering ID from the requester module"
  type        = string
}

##
# Define transit gateway vpc attachment
##
resource "aws_ec2_transit_gateway_vpc_attachment" "attachment" {
  subnet_ids                                      = var.SUBNET_IDS
  transit_gateway_id                              = var.TRANSIT_GATEWAY_ID
  vpc_id                                          = var.networking["VpcId"]
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-attachment",
      "Object", "aws_ec2_transit_gateway_vpc_attachment"
    )
  )
}

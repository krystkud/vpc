variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-peering-acc-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
  peering_acc_name = var.PEERING_ACC_NAME == "" ? local.environment_name : var.PEERING_ACC_NAME
}

variable "globals" {
  description = "Provide global variables from common module."
  type        = map(any)
}

variable "ROUTE_TABLE_IDS" {
  type        = list(string)
  description = "Provide list of route tables on which peer CIDR should be propagated."
}

variable "VPC_PEERING_ID" {
  description = "Provide the VPC peering ID from the requester module"
  type        = string
}

variable "PEER_VPC_CIDR" {
  description = "CIDR of the peered VPC"
  type        = string
}

variable "REMOTE_VPC_DNS_RESOLUTION" {
  default     = false
  description = "Set it to true only after peering is Active"
  type        = bool
}

variable "PEERING_ACC_NAME" {
  description = "Name of peering request"
  type        = string 
}
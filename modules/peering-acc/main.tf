##
# Define VPC peering connection - Accepter side
##
resource "aws_vpc_peering_connection_accepter" "peer_connection" {
  count                     = var.VPC_PEERING_ID == true ? 0 : 1
  vpc_peering_connection_id = var.VPC_PEERING_ID
  auto_accept               = true
  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.peering_acc_name}-vpc-peering",
      "Object", "aws_vpc_peering",
      "Side", "Accepter"
    )
  )
}

##
# Define VPC peering connection options
##
resource "aws_vpc_peering_connection_options" "peer_connection_options" {
  count                     = var.VPC_PEERING_ID == true ? 0 : 1
  vpc_peering_connection_id = aws_vpc_peering_connection_accepter.peer_connection[0].id

  accepter {
    allow_remote_vpc_dns_resolution = var.REMOTE_VPC_DNS_RESOLUTION
  }
}

##
# Define routes to peered VPC
##
resource "aws_route" "peer_connection_propagation" {
  count                     = length(var.ROUTE_TABLE_IDS)
  route_table_id            = var.ROUTE_TABLE_IDS[count.index]
  destination_cidr_block    = var.PEER_VPC_CIDR
  vpc_peering_connection_id = var.VPC_PEERING_ID

}

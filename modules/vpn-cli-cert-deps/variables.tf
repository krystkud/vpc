variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-vpn-cli-cert-deps-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "USERS" {
  description = "List of users"
  default     = []
  type        = list(map(string))
}

variable "KMS_KEY_ID" {
  description = "KMS key id to encrypt secrets"
  type        = string
}

variable "LAMBDA_ROLE_ARN" {
  description = "Arn of role of the Lambda which create certificates"
  type        = string
}

variable "ACCOUNT_ID" {
  description = "Account id"
  type        = string
}

variable "LAMBDA_NAME" {
  description = "Cert Lambda ARN"
  type        = string
}

##
# Define cert storage for customer gateway modules
##
data "aws_iam_policy_document" "private_policy" {
  statement {
    effect = "Deny"
    actions = [
      "secretsmanager:GetSecretValue",
      "secretsmanager:PutSecretValue"
    ]
    resources = [
      "*"
    ]
    not_principals {
      identifiers = [
        "arn:aws:sts::${var.ACCOUNT_ID}:assumed-role/${split("/", var.LAMBDA_ROLE_ARN)[1]}/${var.LAMBDA_NAME}",
        "arn:aws:iam::${var.ACCOUNT_ID}:root",
      var.LAMBDA_ROLE_ARN]

      type = "AWS"
    }
  }
}


resource "aws_secretsmanager_secret" "ca" {
  name                    = "${local.environment_name}-cacrt"
  kms_key_id              = var.KMS_KEY_ID
  policy                  = data.aws_iam_policy_document.private_policy.json
  recovery_window_in_days = 0
  tags = merge(
    local.default_tags,
    map(
      "Object", "aws_secretsmanager_secret"
    )
  )
}

resource "aws_secretsmanager_secret" "ca_key" {
  name                    = "${local.environment_name}-cakey"
  kms_key_id              = var.KMS_KEY_ID
  policy                  = data.aws_iam_policy_document.private_policy.json
  recovery_window_in_days = 0
  tags = merge(
    local.default_tags,
    map(
      "Object", "aws_secretsmanager_secret"
    )
  )
}

data "aws_iam_policy_document" "crl_policy" {
  statement {
    effect = "Deny"
    actions = [
      "secretsmanager:PutSecretValue"
    ]
    resources = [
      "*"
    ]
    not_principals {
      identifiers = [
        "arn:aws:sts::${var.ACCOUNT_ID}:assumed-role/${split("/", var.LAMBDA_ROLE_ARN)[1]}/${var.LAMBDA_NAME}",
        "arn:aws:iam::${var.ACCOUNT_ID}:root",
      var.LAMBDA_ROLE_ARN]

      type = "AWS"
    }
  }
}

resource "aws_secretsmanager_secret" "crl" {
  name                    = "${local.environment_name}-crl"
  kms_key_id              = var.KMS_KEY_ID
  policy                  = data.aws_iam_policy_document.crl_policy.json
  recovery_window_in_days = 0
  tags = merge(
    local.default_tags,
    map(
      "Object", "aws_secretsmanager_secret"
    )
  )
}

data "aws_iam_policy_document" "user_policy" {
  count = length(var.USERS)
  statement {
    effect = "Deny"
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    resources = [
      "*"
    ]
    not_principals {
      identifiers = [
        var.USERS[count.index]["arn"],
        "arn:aws:iam::${var.ACCOUNT_ID}:root",
        "arn:aws:iam::${var.ACCOUNT_ID}:role/${split("/", var.USERS[count.index]["arn"])[1]}",
        var.LAMBDA_ROLE_ARN,
      "arn:aws:sts::${var.ACCOUNT_ID}:assumed-role/${split("/", var.LAMBDA_ROLE_ARN)[1]}/${var.LAMBDA_NAME}"]

      type = "AWS"
    }
  }
  statement {
    effect = "Deny"
    actions = [
      "secretsmanager:PutSecretValue"
    ]
    resources = [
      "*"
    ]
    not_principals {
      identifiers = [
        "arn:aws:iam::${var.ACCOUNT_ID}:root",
        var.LAMBDA_ROLE_ARN,
      "arn:aws:sts::${var.ACCOUNT_ID}:assumed-role/${split("/", var.LAMBDA_ROLE_ARN)[1]}/${var.LAMBDA_NAME}"]

      type = "AWS"
    }
  }
}

resource "aws_secretsmanager_secret" "usercert" {
  count                   = length(var.USERS)
  name                    = "${local.environment_name}-${var.USERS[count.index]["name"]}-usercrt"
  kms_key_id              = var.KMS_KEY_ID
  policy                  = data.aws_iam_policy_document.user_policy[count.index].json
  recovery_window_in_days = 0
  tags = merge(
    local.default_tags,
    map(
      "Object", "aws_secretsmanager_secret"
    )
  )
}

resource "aws_secretsmanager_secret" "userkey" {
  count                   = length(var.USERS)
  name                    = "${local.environment_name}-${var.USERS[count.index]["name"]}-userkey"
  kms_key_id              = var.KMS_KEY_ID
  policy                  = data.aws_iam_policy_document.user_policy[count.index].json
  recovery_window_in_days = 0
  tags = merge(
    local.default_tags,
    map(
      "Object", "aws_secretsmanager_secret"
    )
  )
}

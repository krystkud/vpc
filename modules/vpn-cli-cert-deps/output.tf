output "ca_secret_name" {
  value       = aws_secretsmanager_secret.ca.name
  description = "CA cert secret name"
}
output "cakey_secret_name" {
  value       = aws_secretsmanager_secret.ca_key.name
  description = "CA key secret name"
}

output "users_certs_secret_names" {
  value       = aws_secretsmanager_secret.usercert.*.name
  description = "Users cert secret names"
}

output "users_keys_secret_names" {
  value       = aws_secretsmanager_secret.userkey.*.name
  description = "Users key secret names"
}

output "crl_secret_name" {
  value       = aws_secretsmanager_secret.crl.name
  description = "CRL secret name"
}

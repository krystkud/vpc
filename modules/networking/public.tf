##
# Define public network
##
resource "aws_subnet" "public" {
  count             = length(var.PUBLIC_CIDR_BLOCKS)
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.PUBLIC_CIDR_BLOCKS[count.index]
  availability_zone = local.availability_zone[count.index % length(local.availability_zone)]

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-public-${local.availability_zone[count.index % length(local.availability_zone)]}",
      "AZ", var.PUBLIC_CIDR_BLOCKS[count.index],
      "Object", "aws_subnet",
      "Tier", "public",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster", "shared",
      "kubernetes.io/role/elb", "1" #string_type_lint_skip
    ),
    { for suffix in var.EKS_SUFFIX : "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${suffix}" => "shared" }
  )
}

##
# Define public route table
##
resource "aws_route_table" "public" {
  count  = length(var.PUBLIC_CIDR_BLOCKS) > 0 ? 1 : 0
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-public-route-table",
      "Object", "aws_route_table"
    )
  )
}

##
# Associate public route table
##
resource "aws_route_table_association" "public" {
  count          = length(var.PUBLIC_CIDR_BLOCKS)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public[0].id
}

##
# Define additional public route
##
resource "aws_route" "public" {
  count                  = var.USE_INTERNET_GATEWAY == true ? 1 : 0
  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0" #tfsec:ignore:AWS050
  gateway_id             = aws_internet_gateway.igw[0].id
  depends_on             = [aws_route_table.public]
}

##
# Main definition of elastic ip / nat
##
resource "aws_eip" "public" {
  count = var.USE_NAT_GATEWAY == true && (length(var.SKIP_NAT_ON_REGION) == 0 || !contains(var.SKIP_NAT_ON_REGION, var.globals["Region"])) ? (var.SINGLE_NAT_GATEWAY == true ? 1 : length(var.PUBLIC_CIDR_BLOCKS)) : 0
  vpc   = true

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-eip-${count.index + 1}",
      "Tier", "public",
      "Object", "aws_eip"
    )
  )
}

##
# Define nat gateway
##
resource "aws_nat_gateway" "public" {
  count         = var.USE_NAT_GATEWAY == true && (length(var.SKIP_NAT_ON_REGION) == 0 || !contains(var.SKIP_NAT_ON_REGION, var.globals["Region"])) ? (var.SINGLE_NAT_GATEWAY == true ? 1 : length(var.PUBLIC_CIDR_BLOCKS)) : 0
  allocation_id = aws_eip.public[count.index].id
  subnet_id     = aws_subnet.public[count.index].id
  depends_on    = [aws_subnet.public]

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-nat-${count.index + 1}",
      "Object", "aws_nat_gateway"
    )
  )
}

##
# Define main ACL (all subnets will be assigned to this ACL)
##
resource "aws_network_acl" "public" {
  vpc_id = aws_vpc.main.id

  subnet_ids = concat(aws_subnet.public[*].id)

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-public-network-acl",
      "Object", "aws_default_network_acl"
    )
  )
}

##
# Define main ACL optional rules (by default allow all as security may be based on security groups)
##
resource "aws_network_acl_rule" "public_default_ingress_acl_rules" {
  count          = var.CREATE_DEFAULT_ACL_RULES == true ? 1 : 0
  network_acl_id = aws_network_acl.public.id
  rule_number    = 30000
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0" #tfsec:ignore:AWS050
  from_port      = 0
  to_port        = 0
  protocol       = "-1"
  egress         = false
}

resource "aws_network_acl_rule" "public_default_egress_acl_rules" {
  count          = var.CREATE_DEFAULT_ACL_RULES == true ? 1 : 0
  network_acl_id = aws_network_acl.public.id
  rule_number    = 30000
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0" #tfsec:ignore:AWS050
  from_port      = 0
  to_port        = 0
  protocol       = "-1"
  egress         = true
}

resource "aws_network_acl_rule" "public_ingress_acl_rules" {
  count          = length(var.PUBLIC_INGRESS_ACL_RULES)
  network_acl_id = aws_network_acl.public.id
  rule_number    = keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]
  rule_action    = var.PUBLIC_INGRESS_ACL_RULES[keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]][0]
  cidr_block     = var.PUBLIC_INGRESS_ACL_RULES[keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]][1]
  from_port      = var.PUBLIC_INGRESS_ACL_RULES[keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]][2]
  to_port        = var.PUBLIC_INGRESS_ACL_RULES[keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]][3]
  protocol       = var.PUBLIC_INGRESS_ACL_RULES[keys(var.PUBLIC_INGRESS_ACL_RULES)[count.index]][4]
  egress         = false
}

resource "aws_network_acl_rule" "public_egress_acl_rules" {
  count          = length(var.PUBLIC_EGRESS_ACL_RULES)
  network_acl_id = aws_network_acl.public.id
  rule_number    = keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]
  rule_action    = var.PUBLIC_EGRESS_ACL_RULES[keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]][0]
  cidr_block     = var.PUBLIC_EGRESS_ACL_RULES[keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]][1]
  from_port      = var.PUBLIC_EGRESS_ACL_RULES[keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]][2]
  to_port        = var.PUBLIC_EGRESS_ACL_RULES[keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]][3]
  protocol       = var.PUBLIC_EGRESS_ACL_RULES[keys(var.PUBLIC_EGRESS_ACL_RULES)[count.index]][4]
  egress         = true
}

output "vpc_id" {
  description = "Return vpc id"
  value       = aws_vpc.main.id
}

output "public_availability_zones" {
  description = "Return public availability zones"
  value       = aws_subnet.public[*].availability_zone
}

output "vpc_cidr_block" {
  description = "Return vpc cidr block"
  value       = var.VPC_CIDR_BLOCK
}

output "application_availability_zones" {
  description = "Return application availability zones"
  value       = aws_subnet.application[*].availability_zone
}

output "data_availability_zones" {
  description = "Return data availability zones"
  value       = aws_subnet.data[*].availability_zone
}

output "public_subnets" {
  description = "Return public subnets"
  value       = aws_subnet.public[*].cidr_block
}

output "public_subnet_ids" {
  description = "Return public subnets id"
  value       = aws_subnet.public[*].id
}

output "public_route_table_ids" {
  description = "Return public route tables id"
  value       = aws_route_table.public[*].id
}

output "application_subnets" {
  description = "Return application subnets"
  value       = aws_subnet.application[*].cidr_block
}

output "application_subnet_ids" {
  description = "Return application subnets id"
  value       = aws_subnet.application[*].id
}

output "application_route_table_ids" {
  description = "Return application route tables id"
  value       = aws_route_table.application[*].id
}

output "data_subnets" {
  description = "Return data subnets"
  value       = aws_subnet.data[*].cidr_block
}

output "data_subnet_ids" {
  description = "Return data subnets id"
  value       = aws_subnet.data[*].id
}

output "data_route_table_ids" {
  description = "Return data route tables id"
  value       = aws_route_table.data[*].id
}

output "default_security_group_id" {
  description = "Return default security group id"
  value       = aws_security_group.default_security_group.id
}

output "main_zone_id" {
  description = "Return main local zone id"
  value       = concat(aws_route53_zone.main[*].zone_id, ["DNS_ZONE_NOT_CREATED"])[0]
}

output "main_zone_name" {
  description = "Return main local zone name"
  value       = replace(concat(aws_route53_zone.main[*].name, ["DNS_ZONE_NOT_CREATED"])[0], "/[.]$/", "")
}

output "common" {
  description = "Return common networking variables used by other modules"
  value       = local.common
}

output "dopt" {
  description = "The ID of the DHCP Options Set"
  value       = aws_vpc_dhcp_options.default.id
}

output "nat_public_ips" {
  value       = aws_nat_gateway.public.*.public_ip
  description = "The public IP address of the NAT Gateway"
}

output "nat_private_ips" {
  value       = aws_nat_gateway.public.*.private_ip
  description = "The private IP address of the NAT Gateway"
}

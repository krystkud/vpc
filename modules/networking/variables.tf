variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-networking-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

##
# Define common networking variables
##
locals {
  common = {
    "VpcId"                  = aws_vpc.main.id
    "DefaultSecurityGroupId" = aws_security_group.default_security_group.id
    "VpcCidrBlock"           = var.VPC_CIDR_BLOCK
    "MainZoneId"             = concat(aws_route53_zone.main[*].zone_id, ["DNS_ZONE_NOT_CREATED"])[0]
    "MainZoneName"           = replace(concat(aws_route53_zone.main[*].name, ["DNS_ZONE_NOT_CREATED"])[0], "/[.]$/", "")
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

locals {
  availability_zone = data.aws_availability_zones.available.names[*]
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "VPC_TENANCY" {
  default     = "default"
  description = "Provide type of tenancy for the VPC"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  default     = "10.1.0.0/24"
  description = "Provide cidr block for whole vpc"
  type        = string
}

variable "SSH_CIDR_BLOCKS" {
  default     = []
  type        = list(string)
  description = "Provide additional cidr blocks for ssh ingress"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks, e.g. '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks, e.g. '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks, e.g. '10.1.20.0/24'"
  type        = list(string)
}

variable "APPLICATION_SUBNET_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for application subnets"
  type        = map(any)
}

variable "DATA_SUBNET_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for data subnets"
  type        = map(any)
}

variable "DEFAULT_SG_ADDITIONAL_TAGS" {
  default     = {}
  description = "Additional tags for default security group"
  type        = map(any)
}

variable "USE_NAT_GATEWAY" {
  default     = false
  description = "Define whether nat gateway should be used (valid when SKIP_NAT_ON_REGION is not true)"
  type        = bool
}

variable "SKIP_NAT_ON_REGION" {
  type        = list(any)
  default     = []
  description = "List of regions for which NAT gateway should not be enabled as there are all endpoints available"
}

variable "USE_INTERNET_GATEWAY" {
  default     = true
  description = "Define whether internet gateway should be used"
  type        = bool
}

variable "SINGLE_NAT_GATEWAY" {
  default     = false
  description = "Define whether only single nat gateway should be created"
  type        = bool
}

variable "CREATE_PRIVATE_DNS_ZONE" {
  default     = true
  description = "Define whether private DNS zones should be created"
  type        = bool
}

variable "FLOW_LOGS_ENABLED" {
  default     = false
  description = "Define whether vpc flow logs should be enabled"
  type        = bool
}

variable "FLOW_LOGS_BUCKET_ARN" {
  default     = ""
  description = "Provide vpc flow log s3 bucket arn"
  type        = string
}

variable "FLOW_LOGS_TRAFFIC_TYPE" {
  default     = "REJECT"
  description = "Provide type of vpc traffic to capture (ACCEPT, REJECT, ALL)"
  type        = string
}

variable "CREATE_DEFAULT_ACL_RULES" {
  default     = true
  description = "Define whether default ACL rules should be created. Allow ingress and egress from all IPs and ports (rule number 10000)."
  type        = bool
}

variable "PUBLIC_INGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "PUBLIC_EGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "APPLICATION_INGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "APPLICATION_EGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "DATA_INGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "DATA_EGRESS_ACL_RULES" {
  type        = map(any)
  default     = {}
  description = "Advanced feature. Provide map of additional ingress ACL rules in following format: { \"rule_number\" = [\"allow/deny\", \"cidr\", \"from_port\", \"to_port\", \"protocol\"]}."
}

variable "DOMAIN_NAME_SERVERS" {
  default     = ["AmazonProvidedDNS"]
  type        = list(string)
  description = "(Optional) List of name servers to configure in /etc/resolv.conf. If you want to use the default AWS nameservers you should set this to AmazonProvidedDNS"
}

variable "DOMAIN_NAME" {
  default     = null
  type        = string
  description = "The suffix domain name to use by default when resolving non Fully Qualified Domain Names. In other words, this is what ends up being the search value in the /etc/resolv.conf file"
}

variable "NTP_SERVERS" {
  default     = null
  type        = list(string)
  description = "List of NTP servers to configure"
}

variable "NETBIOS_NAME_SERVERS" {
  default     = null
  type        = list(string)
  description = "List of NETBIOS name servers"
}

variable "NETBIOS_NODE_TYPE" {
  default     = null
  type        = number
  description = "The NetBIOS node type (1, 2, 4, or 8). AWS recommends to specify 2 since broadcast and multicast are not supported in their network"
}

variable "EKS_SUFFIX" {
  default     = []
  type        = list(string)
  description = "List of suffixes used for EKS clusters in this VPC"
}

variable "ADDITIONAL_CIDRS" {
  default     = []
  type        = list(string)
  description = "List of Additional CIDR Blocks for the VPC. Must be compatible with main CIDR, see https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html#vpc-resize"
}

variable "HTTP_ENABLE" {
  default     = true
  type        = bool
  description = "Switch to disable outgoing port 80 traffic"
}

variable "HTTPS_ENABLE" {
  default     = true
  type        = bool
  description = "Switch to disable outgoing port 443 traffic"
}

variable "SALT_ENABLE" {
  default     = true
  type        = bool
  description = "Switch to disable saltstack outgoing traffic"
}


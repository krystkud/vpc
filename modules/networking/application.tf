##
# Define application network
##
resource "aws_subnet" "application" {
  count             = length(var.APPLICATION_CIDR_BLOCKS)
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.APPLICATION_CIDR_BLOCKS[count.index]
  availability_zone = local.availability_zone[count.index % length(local.availability_zone)]

  tags = merge(
    local.default_tags,
    var.APPLICATION_SUBNET_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-application-${local.availability_zone[count.index % length(local.availability_zone)]}",
      "AZ", var.APPLICATION_CIDR_BLOCKS[count.index],
      "Tier", "application",
      "Object", "aws_subnet",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster", "shared",
      "kubernetes.io/role/internal-elb", "1" #string_type_lint_skip
    ),
    { for suffix in var.EKS_SUFFIX : "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${suffix}" => "shared" }
  )

}

##
# Define application route table
##
resource "aws_route_table" "application" {
  count  = length(var.APPLICATION_CIDR_BLOCKS)
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-application-route-table",
      "Tier", "application",
      "Object", "aws_route_table"
    )
  )
}

##
# Associate application route table
##
resource "aws_route_table_association" "application" {
  count          = length(var.APPLICATION_CIDR_BLOCKS)
  subnet_id      = aws_subnet.application[count.index].id
  route_table_id = aws_route_table.application[count.index].id
}

##
# Define additional application route
##
resource "aws_route" "application" {
  count                  = var.USE_NAT_GATEWAY == true && (length(var.SKIP_NAT_ON_REGION) == 0 || !contains(var.SKIP_NAT_ON_REGION, var.globals["Region"])) ? length(var.APPLICATION_CIDR_BLOCKS) : 0
  route_table_id         = aws_route_table.application[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.public[(var.SINGLE_NAT_GATEWAY == true ? 0 : count.index)].id
  depends_on             = [aws_route_table.application]
}

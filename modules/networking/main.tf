##
# Define main vpc
##
resource "aws_vpc" "main" {
  cidr_block           = var.VPC_CIDR_BLOCK
  instance_tenancy     = var.VPC_TENANCY
  enable_dns_hostnames = true

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-vpc",
      "Object", "aws_vpc",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster", "shared"

    ),
    { for suffix in var.EKS_SUFFIX : "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${suffix}" => "shared" }
  )
}

##
# Define vpc flow logs
##
resource "aws_flow_log" "main" {
  count                = var.FLOW_LOGS_ENABLED == true ? 1 : 0
  log_destination      = var.FLOW_LOGS_BUCKET_ARN
  log_destination_type = "s3"
  traffic_type         = var.FLOW_LOGS_TRAFFIC_TYPE
  vpc_id               = aws_vpc.main.id
}

##
# Define vpn dhcp options
##
resource "aws_vpc_dhcp_options" "default" {
  domain_name          = var.DOMAIN_NAME != null ? var.DOMAIN_NAME : "${local.default_tags["Region"]}.compute.internal"
  domain_name_servers  = var.DOMAIN_NAME_SERVERS
  ntp_servers          = var.NTP_SERVERS
  netbios_name_servers = var.NETBIOS_NAME_SERVERS
  netbios_node_type    = var.NETBIOS_NODE_TYPE

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-dhcp-options",
      "Object", "aws_vpc_dhcp_options"
    )
  )

  depends_on = [aws_vpc.main]
}

resource "aws_vpc_dhcp_options_association" "default" {
  vpc_id          = aws_vpc.main.id
  dhcp_options_id = aws_vpc_dhcp_options.default.id

  depends_on = [aws_vpc.main, aws_vpc_dhcp_options.default]
}


##
# Define main vpc internet gateway
##
resource "aws_internet_gateway" "igw" {
  count  = var.USE_INTERNET_GATEWAY == true ? 1 : 0
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-internet-gateway",
      "Object", "aws_internet_gateway"
    )
  )
}

##
# Define main vpc route table
##
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-main-route-table",
      "Object", "aws_route_table"
    )
  )

  depends_on = [aws_vpc.main]
}

##
# Associate vpc with main route table
##
resource "aws_main_route_table_association" "main_association" {
  vpc_id         = aws_vpc.main.id
  route_table_id = aws_route_table.main.id
  depends_on     = [aws_route_table.main]
}

##
# Change ingress and egress rules of default security group
##
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-default-security-group",
      "Object", "aws_default_security_group"
    )
  )
}

resource "aws_vpc_ipv4_cidr_block_association" "additional_cidr" {
  count      = length(var.ADDITIONAL_CIDRS)
  vpc_id     = aws_vpc.main.id
  cidr_block = var.ADDITIONAL_CIDRS[count.index]
}

##
# Define default network acl (This ACL will deny all traffic and won't be assigned to any data, public and application subnet)
##
resource "aws_default_network_acl" "default" {
  default_network_acl_id = aws_vpc.main.default_network_acl_id

  # https://github.com/terraform-providers/terraform-provider-aws/issues/346
  lifecycle {
    ignore_changes = [subnet_ids]
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-default-network-acl",
      "Object", "aws_default_network_acl"
    )
  )
}

##
# Define default_security_group security group
##
resource "aws_security_group" "default_security_group" {
  name        = "${local.environment_name}-default-security-group"
  description = "Allow all inbound traffic"
  vpc_id      = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    var.DEFAULT_SG_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-default-security-group",
      "Object", "aws_security_group"
    )
  )
}

##
# Define default_security_group security rules
##
resource "aws_security_group_rule" "default_security_group_ingress_22" {
  count             = length(var.SSH_CIDR_BLOCKS) == 0 ? 0 : 1
  description       = "Allow incoming network flow on port 22/tcp"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = concat([var.VPC_CIDR_BLOCK], var.SSH_CIDR_BLOCKS)
  security_group_id = aws_security_group.default_security_group.id
}

resource "aws_security_group_rule" "default_security_group_egress_80" {
  count             = var.HTTP_ENABLE == true ? 1 : 0
  description       = "Allow outgoing network flow on port 80/tcp"
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS007
  security_group_id = aws_security_group.default_security_group.id
}

resource "aws_security_group_rule" "default_security_group_egress_443" {
  count             = var.HTTPS_ENABLE ? 1 : 0
  description       = "Allow outgoing network flow on port 443/tcp"
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"] #tfsec:ignore:AWS007
  security_group_id = aws_security_group.default_security_group.id
}

resource "aws_security_group_rule" "default_security_group_egress_4505_4506" {
  count             = var.SALT_ENABLE ? 1 : 0
  description       = "Allow outgoing network flow on port 4505-4506/tcp"
  type              = "egress"
  from_port         = 4505
  to_port           = 4506
  protocol          = "tcp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.default_security_group.id
}

resource "aws_security_group_rule" "default_security_group_egress_53" {
  description       = "Allow outgoing network flow on port 53/udp"
  type              = "egress"
  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.default_security_group.id
}

resource "aws_security_group_rule" "default_security_group_egress_123" {
  description       = "Allow outgoing network flow on port 123/udp"
  type              = "egress"
  from_port         = 123
  to_port           = 123
  protocol          = "udp"
  cidr_blocks       = [var.VPC_CIDR_BLOCK]
  security_group_id = aws_security_group.default_security_group.id
}

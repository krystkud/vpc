##
# Define local dns zone
##
resource "aws_route53_zone" "main" {
  count         = var.CREATE_PRIVATE_DNS_ZONE == true ? 1 : 0
  name          = "${lower(local.default_tags["Environment"])}.${lower(replace(local.default_tags["Project"], " ", "-"))}."
  comment       = "Created for ${lower(local.environment_name)}"
  force_destroy = true

  vpc {
    vpc_id = aws_vpc.main.id
  }

  lifecycle {
    ignore_changes = [vpc]
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.default_tags["Environment"]}.${local.default_tags["Project"]}-zone",
      "Object", "aws_route53_zone"
    )
  )
}

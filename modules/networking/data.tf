##
# Define data network
##
resource "aws_subnet" "data" {
  count             = length(var.DATA_CIDR_BLOCKS)
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.DATA_CIDR_BLOCKS[count.index]
  availability_zone = local.availability_zone[count.index % length(local.availability_zone)]

  tags = merge(
    local.default_tags,
    var.DATA_SUBNET_ADDITIONAL_TAGS,
    map(
      "Name", "${local.environment_name}-data-${local.availability_zone[count.index % length(local.availability_zone)]}",
      "AZ", var.DATA_CIDR_BLOCKS[count.index],
      "Object", "aws_subnet",
      "Tier", "data",
      "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster", "shared",
      "kubernetes.io/role/internal-elb", "1" #string_type_lint_skip
    ),
    { for suffix in var.EKS_SUFFIX : "kubernetes.io/cluster/${lower(local.default_tags["Project"])}-${lower(local.default_tags["Environment"])}-cluster${suffix}" => "shared" }
  )
}

##
# Define data route table
##
resource "aws_route_table" "data" {
  count  = length(var.DATA_CIDR_BLOCKS)
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-data-route-table",
      "Tier", "data",
      "Object", "aws_route_table"
    )
  )
}

##
# Associate data route table
##
resource "aws_route_table_association" "data" {
  count          = length(var.DATA_CIDR_BLOCKS)
  subnet_id      = aws_subnet.data[count.index].id
  route_table_id = aws_route_table.data[count.index].id
}

##
# Define additional data route
##
resource "aws_route" "data" {
  count                  = var.USE_NAT_GATEWAY == true && (length(var.SKIP_NAT_ON_REGION) == 0 || !contains(var.SKIP_NAT_ON_REGION, var.globals["Region"])) ? length(var.DATA_CIDR_BLOCKS) : 0
  route_table_id         = aws_route_table.data[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.public[(var.SINGLE_NAT_GATEWAY == true ? 0 : count.index)].id
  depends_on             = [aws_route_table.data]
}

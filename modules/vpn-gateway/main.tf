##
# Define vpn gateway
##
resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = var.VPC_ID

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-vgw-${var.VPN_GW_NAME_SUFFIX}",
      "Object", "aws_vpn_gateway"
    )
  )
}

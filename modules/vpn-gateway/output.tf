output "vpn_gateway_id" {
  description = "Return id of VPN gateway."
  value       = aws_vpn_gateway.vpn_gateway.id
}

output "vpn_gateway_amzn_asn" {
  description = "Return Amazon side ASN of VPN gateway."
  value       = aws_vpn_gateway.vpn_gateway.amazon_side_asn
}

variable "TRANSIT_GATEWAY_ID" {
  description = "Provide id of the transit gateway"
  type        = string
}

variable "ROUTE_TABLE_IDS" {
  description = "Provide route tables ids"
  type        = list(string)
}

variable "DESTINATION_CIDR_BLOCK" {
  default     = ""
  description = "Provide destination cidr block"
  type        = string
}

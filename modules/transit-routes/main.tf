##
# Define dedicated routes to transit gateway
##
resource "aws_route" "attachment" {
  count                  = length(var.ROUTE_TABLE_IDS)
  route_table_id         = var.ROUTE_TABLE_IDS[count.index]
  destination_cidr_block = var.DESTINATION_CIDR_BLOCK
  transit_gateway_id     = var.TRANSIT_GATEWAY_ID
}

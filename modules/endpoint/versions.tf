terraform {
  required_version = ">= 0.12.0"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    # Update it only if changes in this module really required features provided by this version
    # Don't update blindly as we will use latest anyway.
    #aws = ">=2.0"
    aws = {
      source  = "hashicorp/aws"
      version = ">=2.0"
    } #bug which make fail of auto_accept
    template = {
      source  = "hashicorp/template"
      version = ">= 2.1.2"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 2.1.0"
    }
  }
}

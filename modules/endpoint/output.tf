output "gateway_id" {
  description = "Return vpc gateway id"
  value       = aws_vpc_endpoint.gateway[*].id
}

output "interface_id" {
  description = "Return vpc interface id"
  value       = aws_vpc_endpoint.interface[*].id
}

output "dns_entry" {
  description = "Return DNS entry."
  value       = flatten(concat(aws_vpc_endpoint.gateway[*].dns_entry, aws_vpc_endpoint.interface[*].dns_entry))
}

output "endpoint_security_group_id" {
  description = "Return id of security group assigned to endpoint."
  value       = aws_security_group.endpoint_security_group[*].id
}

output "gateway_state" {
  description = "Return gateway endpoint status."
  value       = aws_vpc_endpoint.gateway.*.state
}

output "interface_state" {
  description = "Return interface endpoint status."
  value       = aws_vpc_endpoint.interface.*.state
}

##
# Define endpoint_security_group (default) security group
##
resource "aws_security_group" "endpoint_security_group" {
  count       = var.ENDPOINT_TYPE == "Interface" ? 1 : 0
  name_prefix = "${local.environment_name}-endpoint"
  description = "Allow all inbound traffic"
  vpc_id      = data.aws_vpc.main.id

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint",
      "Object", "aws_security_group"
    )
  )
}

##
# Define k8s-default security rules
##

resource "aws_security_group_rule" "endpoint_security_group_ingress_default" {
  count             = var.ADD_DEFAULT_SG_RULE == true ? length(var.INGRESS_PORTS) : 0
  description       = "Allow incoming network flow on port ${var.INGRESS_PORTS[count.index]}/tcp"
  type              = "ingress"
  from_port         = var.INGRESS_PORTS[count.index]
  to_port           = var.INGRESS_PORTS[count.index]
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.main.cidr_block]
  security_group_id = aws_security_group.endpoint_security_group[0].id
}

resource "aws_security_group_rule" "endpoint_security_group_ingress_rule_ip" {
  count             = length(var.SECURITY_GROUP_INGRESS_IP) > 0 ? length(var.INGRESS_PORTS) : 0
  description       = "Allow incoming network flow on port ${var.INGRESS_PORTS[count.index]}/tcp"
  type              = "ingress"
  from_port         = var.INGRESS_PORTS[count.index]
  to_port           = var.INGRESS_PORTS[count.index]
  protocol          = "tcp"
  cidr_blocks       = var.SECURITY_GROUP_INGRESS_IP
  security_group_id = aws_security_group.endpoint_security_group[0].id
}

resource "aws_security_group_rule" "endpoint_security_group_ingress_rule_sg" {
  count                    = length(var.SOURCE_INGRESS_SECURITY_GROUPS)
  description              = "Allow incoming network flow on port ${split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]}/tcp."
  type                     = "ingress"
  from_port                = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]
  to_port                  = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[1]
  protocol                 = "tcp"
  source_security_group_id = split(":", var.SOURCE_INGRESS_SECURITY_GROUPS[count.index])[0]
  security_group_id        = aws_security_group.endpoint_security_group[0].id
}

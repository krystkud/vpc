variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-endpoint-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

data "aws_vpc" "main" {
  id = var.networking["VpcId"]
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module"
  type        = map(any)
}

variable "ENDPOINT_TYPE" {
  default     = "Gateway"
  description = "Define endpoint type, ex Gateway or Interface"
  type        = string
}

variable "SERVICE_NAME" {
  description = "Define service name, in the form com.amazonaws.region.service for AWS services"
  type        = string
}

variable "ROUTE_TABLE_IDS" {
  default     = []
  description = "Define one or more route table IDs. Applicable for endpoints of type Gateway."
  type        = list(string)
}

variable "SUBNET_IDS" {
  default     = []
  description = "Define id of one or more subnets in which to create a network interface for the endpoint. Applicable for endpoints of type Interface."
  type        = list(string)
}

variable "AUTO_ACCEPT" {
  default     = false
  description = "Accept the VPC endpoint (the VPC endpoint and service need to be in the same AWS account)."
  type        = bool
}

variable "ENABLED_PRIVATE_DNS" {
  default     = true
  description = "Define whether private hosted zone should be associated with the vpc (AWS services or AWS Marketplace partner services only)"
  type        = bool
}

variable "DNS_NAME" {
  default     = ""
  description = "Provide full domain name which should be created in private dns zone"
  type        = string
}

variable "INGRESS_PORTS" {
  default     = []
  type        = list(string)
  description = "Provide ingress port which should be opened for this endpoint"
}

variable "ADD_DEFAULT_SG_RULE" {
  default     = true
  description = "Define whether endpoint should be accessible from VPC CIDR."
  type        = bool
}

variable "SECURITY_GROUP_INGRESS_IP" {
  default     = []
  type        = list(any)
  description = "Provide a list of additional IPs, which should be allowed for ingress traffic."
}

variable "SOURCE_INGRESS_SECURITY_GROUPS" {
  type        = list(string)
  default     = []
  description = "Provide a list of additional source security groups with port, which should be allowed for ingress traffic. Example: ['sg-3123123123:80', 'sg-3123123123:443']"
}

variable "SINGLE_INTERFACE" {
  type        = bool
  default     = true
  description = "Enable single interface for all subnets for endpoints (On production should be set to `false`)"
}

variable "ENABLE_ON_REGION" {
  type        = list(any)
  default     = []
  description = "List of regions for which endpoint is available"
}

variable "GATEWAY_ENDPOINT_POLICY" {
  type        = string
  default     = <<-EOT
    {
      "Statement": [
          {
              "Action": "*",
              "Effect": "Allow",
              "Resource": "*",
              "Principal": "*"
          }
      ],
  "Version"    : "2008-10-17"
    }
  EOT
  description = "Provide json with security policy for interface endpoint."
}

##
# Define vpc endpoint: Gateway
##
resource "aws_vpc_endpoint" "gateway" {
  count             = var.ENDPOINT_TYPE == "Gateway" ? 1 : 0
  vpc_id            = var.networking["VpcId"]
  vpc_endpoint_type = var.ENDPOINT_TYPE
  service_name      = var.SERVICE_NAME
  auto_accept       = var.AUTO_ACCEPT
  route_table_ids   = var.ROUTE_TABLE_IDS
  policy            = var.GATEWAY_ENDPOINT_POLICY

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint-${var.SERVICE_NAME}",
      "Object", "aws_vpc_endpoint"
    )
  )
}

##
# Define vpc endpoint: Interface
##
resource "aws_vpc_endpoint" "interface" {
  count               = var.ENDPOINT_TYPE == "Interface" && (length(var.ENABLE_ON_REGION) == 0 || contains(var.ENABLE_ON_REGION, var.globals["Region"])) ? 1 : 0
  vpc_id              = var.networking["VpcId"]
  vpc_endpoint_type   = var.ENDPOINT_TYPE
  service_name        = var.SERVICE_NAME
  auto_accept         = var.AUTO_ACCEPT
  security_group_ids  = [aws_security_group.endpoint_security_group[0].id]
  subnet_ids          = var.SINGLE_INTERFACE && var.ENDPOINT_TYPE == "Interface" ? [var.SUBNET_IDS[0]] : var.SUBNET_IDS
  private_dns_enabled = var.ENABLED_PRIVATE_DNS

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint-${var.SERVICE_NAME}",
      "Object", "aws_vpc_endpoint"
    )
  )
}

resource "aws_route53_zone" "private" {
  count = var.DNS_NAME != "" && (length(var.ENABLE_ON_REGION) == 0 || contains(var.ENABLE_ON_REGION, var.globals["Region"])) ? 1 : 0
  name  = "${lower(var.DNS_NAME)}."

  vpc {
    vpc_id = var.networking["VpcId"]
  }

  lifecycle {
    ignore_changes = [vpc]
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${var.DNS_NAME}-zone",
      "Object", "aws_route53_zone"
    )
  )
}

##
# Define record which should cover endpoint
##
resource "aws_route53_record" "endpoint" {
  count   = var.DNS_NAME != "" && (length(var.ENABLE_ON_REGION) == 0 || contains(var.ENABLE_ON_REGION, var.globals["Region"])) ? 1 : 0
  zone_id = aws_route53_zone.private[0].zone_id
  name    = var.DNS_NAME
  type    = "A"

  alias {
    name                   = lookup(aws_vpc_endpoint.interface[0].dns_entry[0], "dns_name")
    zone_id                = lookup(aws_vpc_endpoint.interface[0].dns_entry[0], "hosted_zone_id")
    evaluate_target_health = true
  }
}

output "vpn_connection_id" {
  description = "Return id of VPN connection."
  value       = aws_vpn_connection.vpn_connection.id
}

output "vpn_customer_gateway_id" {
  description = "Return id of VPN Customer Gateway."
  value       = aws_customer_gateway.customer_gateway.id
}

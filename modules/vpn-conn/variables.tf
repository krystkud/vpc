variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-vpn-conn-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "CUSTOMER_GATEWAY_IP" {
  description = "Provide customer gateway ip address"
  type        = string
}

variable "CUSTOMER_CIDR_BLOCKS" {
  type        = list(string)
  description = "Provide customer cidr block list"
}

variable "VPN_GATEWAY_ID" {
  description = "Provide id of VPN gateway."
  type        = string
}

variable "ROUTE_TABLE_IDS" {
  type        = list(string)
  description = "Provide list of route tables on which Customer CIDRs should be propagated"
}

variable "USE_ONLY_STATIC_ROUTE" {
  default     = true
  description = "Boolean variable if VPN connection should use only static routes"
  type        = bool
}

variable "CUSTOMER_GATEWAY_ASN" {
  default     = 65000
  description = "Provide ASN for Customer Gateway."
  type        = number
}

variable "CGW_NAME_SUFFIX" {
  description = "Provide suffix for Customer Gateway name."
  type        = string
}

variable "VPN_NAME_SUFFIX" {
  description = "Provide suffix for VPN connection name."
  type        = string
}

##
# TODO: Once terraform 14 is widely used in IDEMIA, type of all of the fields below should be switched to optional()
# See: https://www.terraform.io/docs/language/expressions/type-constraints.html#experimental-optional-object-type-attributes
# For example: inside_cidr = optional(string)
#
# Unfortunately right now, if you would like to use AWS default settings for any of the options below, you have to
# set it to null, while invoking the module.
#
##
variable "TUNNEL_OPTIONS" {
  description = "Provide list of objects with tunnel options."
  type = map(object({
    inside_cidr                  = string
    startup_action               = string
    preshared_key                = string
    dpd_timeout_action           = string
    dpd_timeout_seconds          = number
    ike_versions                 = list(string)
    phase1_dh_group_numbers      = list(number)
    phase1_encryption_algorithms = list(string)
    phase1_integrity_algorithms  = list(string)
    phase1_lifetime_seconds      = number
    phase2_dh_group_numbers      = list(number)
    phase2_encryption_algorithms = list(string)
    phase2_integrity_algorithms  = list(string)
    phase2_lifetime_seconds      = number
    rekey_fuzz_percentage        = number
    rekey_margin_time_seconds    = number
    replay_window_size           = number
  }))
  default = {
    tunnel1 = {
      inside_cidr                  = null
      startup_action               = null
      preshared_key                = null
      dpd_timeout_action           = null
      dpd_timeout_seconds          = null
      ike_versions                 = null
      phase1_dh_group_numbers      = null
      phase1_encryption_algorithms = null
      phase1_integrity_algorithms  = null
      phase1_lifetime_seconds      = null
      phase2_dh_group_numbers      = null
      phase2_encryption_algorithms = null
      phase2_integrity_algorithms  = null
      phase2_lifetime_seconds      = null
      rekey_fuzz_percentage        = null
      rekey_margin_time_seconds    = null
      replay_window_size           = null
    },
    tunnel2 = {
      inside_cidr                  = null
      startup_action               = null
      preshared_key                = null
      dpd_timeout_action           = null
      dpd_timeout_seconds          = null
      ike_versions                 = null
      phase1_dh_group_numbers      = null
      phase1_encryption_algorithms = null
      phase1_integrity_algorithms  = null
      phase1_lifetime_seconds      = null
      phase2_dh_group_numbers      = null
      phase2_encryption_algorithms = null
      phase2_integrity_algorithms  = null
      phase2_lifetime_seconds      = null
      rekey_fuzz_percentage        = null
      rekey_margin_time_seconds    = null
      replay_window_size           = null
    }
  }
}

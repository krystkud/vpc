##
# Define customer gateway
##
resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = var.CUSTOMER_GATEWAY_ASN
  ip_address = var.CUSTOMER_GATEWAY_IP
  type       = "ipsec.1"

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cgw-${var.CGW_NAME_SUFFIX}",
      "Object", "aws_customer_gateway"
    )
  )
}

##
# Define vpn connection
##
resource "aws_vpn_connection" "vpn_connection" {
  vpn_gateway_id      = var.VPN_GATEWAY_ID
  customer_gateway_id = aws_customer_gateway.customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = var.USE_ONLY_STATIC_ROUTE

  tunnel1_inside_cidr                  = var.TUNNEL_OPTIONS.tunnel1.inside_cidr
  tunnel2_inside_cidr                  = var.TUNNEL_OPTIONS.tunnel2.inside_cidr
  tunnel1_preshared_key                = var.TUNNEL_OPTIONS.tunnel1.preshared_key
  tunnel2_preshared_key                = var.TUNNEL_OPTIONS.tunnel2.preshared_key
  tunnel1_dpd_timeout_action           = var.TUNNEL_OPTIONS.tunnel1.dpd_timeout_action
  tunnel2_dpd_timeout_action           = var.TUNNEL_OPTIONS.tunnel2.dpd_timeout_action
  tunnel1_dpd_timeout_seconds          = var.TUNNEL_OPTIONS.tunnel1.dpd_timeout_seconds
  tunnel2_dpd_timeout_seconds          = var.TUNNEL_OPTIONS.tunnel2.dpd_timeout_seconds
  tunnel1_ike_versions                 = var.TUNNEL_OPTIONS.tunnel1.ike_versions
  tunnel2_ike_versions                 = var.TUNNEL_OPTIONS.tunnel2.ike_versions
  tunnel1_phase1_dh_group_numbers      = var.TUNNEL_OPTIONS.tunnel1.phase1_dh_group_numbers
  tunnel2_phase1_dh_group_numbers      = var.TUNNEL_OPTIONS.tunnel2.phase1_dh_group_numbers
  tunnel1_phase1_encryption_algorithms = var.TUNNEL_OPTIONS.tunnel1.phase1_encryption_algorithms
  tunnel2_phase1_encryption_algorithms = var.TUNNEL_OPTIONS.tunnel2.phase1_encryption_algorithms
  tunnel1_phase1_integrity_algorithms  = var.TUNNEL_OPTIONS.tunnel1.phase1_integrity_algorithms
  tunnel2_phase1_integrity_algorithms  = var.TUNNEL_OPTIONS.tunnel2.phase1_integrity_algorithms
  tunnel1_phase1_lifetime_seconds      = var.TUNNEL_OPTIONS.tunnel1.phase1_lifetime_seconds
  tunnel2_phase1_lifetime_seconds      = var.TUNNEL_OPTIONS.tunnel2.phase1_lifetime_seconds
  tunnel1_phase2_dh_group_numbers      = var.TUNNEL_OPTIONS.tunnel1.phase2_dh_group_numbers
  tunnel2_phase2_dh_group_numbers      = var.TUNNEL_OPTIONS.tunnel2.phase2_dh_group_numbers
  tunnel1_phase2_encryption_algorithms = var.TUNNEL_OPTIONS.tunnel1.phase2_encryption_algorithms
  tunnel2_phase2_encryption_algorithms = var.TUNNEL_OPTIONS.tunnel2.phase2_encryption_algorithms
  tunnel1_phase2_integrity_algorithms  = var.TUNNEL_OPTIONS.tunnel1.phase2_integrity_algorithms
  tunnel2_phase2_integrity_algorithms  = var.TUNNEL_OPTIONS.tunnel2.phase2_integrity_algorithms
  tunnel1_phase2_lifetime_seconds      = var.TUNNEL_OPTIONS.tunnel1.phase2_lifetime_seconds
  tunnel2_phase2_lifetime_seconds      = var.TUNNEL_OPTIONS.tunnel2.phase2_lifetime_seconds
  tunnel1_rekey_fuzz_percentage        = var.TUNNEL_OPTIONS.tunnel1.rekey_fuzz_percentage
  tunnel2_rekey_fuzz_percentage        = var.TUNNEL_OPTIONS.tunnel2.rekey_fuzz_percentage
  tunnel1_rekey_margin_time_seconds    = var.TUNNEL_OPTIONS.tunnel1.rekey_margin_time_seconds
  tunnel2_rekey_margin_time_seconds    = var.TUNNEL_OPTIONS.tunnel2.rekey_margin_time_seconds
  tunnel1_replay_window_size           = var.TUNNEL_OPTIONS.tunnel1.replay_window_size
  tunnel2_replay_window_size           = var.TUNNEL_OPTIONS.tunnel2.replay_window_size
  tunnel1_startup_action               = var.TUNNEL_OPTIONS.tunnel1.startup_action
  tunnel2_startup_action               = var.TUNNEL_OPTIONS.tunnel2.startup_action

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-ipsec-${var.VPN_NAME_SUFFIX}",
      "Object", "aws_vpn_connection"
    )
  )
}

##
# Define customer connection route
##
resource "aws_vpn_connection_route" "customer" {
  count                  = var.USE_ONLY_STATIC_ROUTE == true ? length(var.CUSTOMER_CIDR_BLOCKS) : 0
  vpn_connection_id      = aws_vpn_connection.vpn_connection.id
  destination_cidr_block = var.CUSTOMER_CIDR_BLOCKS[count.index]
}

##
# Define customer CIDRs propagation
##
resource "aws_vpn_gateway_route_propagation" "route_propagation" {
  count          = length(var.ROUTE_TABLE_IDS)
  vpn_gateway_id = var.VPN_GATEWAY_ID
  route_table_id = var.ROUTE_TABLE_IDS[count.index]
}

variable "MODULE_VERSION" {
  default     = "2.1.0"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-vpc-modules-transit-gateway-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map(any)
}

variable "networking" {
  description = "Provide common networking variables for other module"
  type        = map(any)
}

variable "AUTO_ACCEPT_SHARED_ATTACHMENTS" {
  default     = "enable"
  description = "Define if resource attachment requests are automatically accepted"
  type        = string
}

variable "ALLOWED_PRINCIPALS" {
  default     = []
  description = "Provide list of whitelisted aws accounts id"
  type        = list(string)
}

variable "SUBNET_IDS" {
  description = "Provide list of the subnet to launch resources in"
  type        = list(string)
}

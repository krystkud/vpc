output "transit_gateway_id" {
  description = "Return transit gateway id"
  value       = aws_ec2_transit_gateway.gateway.id
}

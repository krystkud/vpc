##
# Define vpc transit gateway
##
resource "aws_ec2_transit_gateway" "gateway" {
  description                     = "${local.environment_name}-tgw"
  auto_accept_shared_attachments  = var.AUTO_ACCEPT_SHARED_ATTACHMENTS
  default_route_table_association = "disable"
  default_route_table_propagation = "disable"

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint",
      "Object", "aws_ec2_transit_gateway"
    )
  )
}

##
# Define share transit gateway
##
resource "aws_ram_resource_share" "share" {
  count = length(var.ALLOWED_PRINCIPALS) == 0 ? 0 : 1
  name  = "${local.environment_name}-tgw-share"

  # only from organisation
  allow_external_principals = false

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-endpoint",
      "Object", "aws_ram_resource_share"
    )
  )
}

##
# Define resource access manager with transit gateway
##
resource "aws_ram_resource_association" "association" {
  count              = length(var.ALLOWED_PRINCIPALS) == 0 ? 0 : 1
  resource_arn       = aws_ec2_transit_gateway.gateway.arn
  resource_share_arn = aws_ram_resource_share.share[0].id
}

##
# Define allowed principals
##
resource "aws_ram_principal_association" "principals" {
  count              = length(var.ALLOWED_PRINCIPALS)
  principal          = var.ALLOWED_PRINCIPALS[count.index]
  resource_share_arn = aws_ram_resource_share.share[0].id
}

##
# Define transit gateway vpc attachment (current vpc)
##
resource "aws_ec2_transit_gateway_vpc_attachment" "attachment" {
  subnet_ids                                      = var.SUBNET_IDS
  transit_gateway_id                              = aws_ec2_transit_gateway.gateway.id
  vpc_id                                          = var.networking["VpcId"]
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-attachment",
      "Object", "aws_ec2_transit_gateway_vpc_attachment"
    )
  )
}

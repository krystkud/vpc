*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/vpn-conn
${region}        %{REGION}

*** Test Cases ***
Test simple
    Set Test Variable  ${example_path}  ${example_path}/simple
    Generate SSH key  ${example_path}  ubuntu-key
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}
    Terraform apply   ${example_path}

    ${terraform_resp}=  Terraform get json module values  ${example_path}  vpn_gateway  aws_vpn_gateway.vpn_gateway
    Set suite variable  ${vpn_gateway_id}  ${terraform_resp['values']['id']}

    AWSession.spawn  ec2  ${region}
    ${aws_api_resp}=  AWSession.get  describe_vpn_gateways
    ${aws_resp}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  VpnGatewayId  ${vpn_gateway_id}
    Should not be empty  ${aws_resp}


Test simple-destroy
    Set Test Variable  ${example_path}  ${example_path}/simple
    Terraform destroy  ${example_path}
    Wait Until Keyword Succeeds  2 min  5 sec  Check resource removal  ${vpn_gateway_id}


*** Keywords ***
Check resource removal
    [Arguments]   ${id}=${None}
    AWSession.spawn  ec2  ${region}
    @{id_list}  Create list  ${id}
    ${aws_api_resp}=  AWSession.get  describe_vpn_gateways  VpnGatewayIds=${id_list}
    Should Be Equal  ${aws_api_resp['VpnGateways'][0]['State']}  deleted  values=True  ignore_case=True

*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/networking
${region}        %{REGION}

*** Test Cases ***
Test simple
    Set Test Variable  ${example_path}  ${example_path}/simple
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}

Test simple-destroy
    Set Test Variable  ${example_path}  ${example_path}/simple
    Terraform destroy  ${example_path}

Test offline mode
    Set Test Variable  ${example_path}  ${example_path}/offline-mode
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}

Test offline mode-destroy
    Set Test Variable  ${example_path}  ${example_path}/offline-mode
    Terraform destroy  ${example_path}

Test multi az
    Set Test Variable  ${example_path}  ${example_path}/multi-az
    Terraform init    ${example_path}
    Terraform get     ${example_path}
    Terraform apply   ${example_path}

Test multi az-destroy
    Set Test Variable  ${example_path}  ${example_path}/multi-az
    Terraform destroy  ${example_path}

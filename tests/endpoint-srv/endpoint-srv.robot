*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot
Resource  resources/kubernetes.robot

Library                SSHLibrary

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/endpoint-srv
${region}        %{REGION}

*** Test Cases ***
Test endpoint service
    Set Test Variable  ${example_path}  ${example_path}/endpoint-srv
    Generate SSH key  ${example_path}  ubuntu-key
    Terraform init    ${example_path}
    Terraform apply   ${example_path}

Test endpoint client
    Set Test Variable  ${example_path}  ${example_path}/endpoint-cli
    Generate SSH key  ${example_path}  ubuntu-key
    Terraform init    ${example_path}

Test endpoint client-destroy
    Set Test Variable  ${example_path}  ${example_path}/endpoint-cli
    Terraform destroy  ${example_path}

Test endpoint service-destroy
    Set Test Variable  ${example_path}  ${example_path}/endpoint-srv
    Terraform destroy  ${example_path}

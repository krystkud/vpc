# Change Log
-------------
## 2.1.4 2021-09-08
TM-467: Added Jenkinsfile.

## 2.1.3 29.07.2021
TM-472: Fix ssh rule creation bug

## 2.1.2 20.07.2021
TM-265: Add name configuration option to VPC peering

## 2.1.1 22.04.2021
TM-242: Allow to disable default flows in default SG

## 2.1.0 26-03-2021
TM-363: ES-1601 NACL per network tier in networking module
TM-309: Add MODULE VERSION and other tags

## 2.0.4 2021-02-04
TM-299: Fixed issue with VPN tunnel options order

## 2.0.3 2021-01-22
TM-299: All of the available tunnel options added to VPN connection module

## 2.0.2 2020-11-30
TM-269: Better tagging for VPC endpoints

## 2.0.1 2020-11-30
TM-229: Additional CIDR Association
TM-239: Make port 22 optional in default VPC group

## 2.0.0 2020-11-06
TM-219: upgrade to terraform 13

## 1.0.1 2020-10-27

TM-158: Fix Loadbalancers on EKS


## 1.0.0 2020-09-02
Release
Tm-162: Added missing tests for vpc.
TM-127: Addedfix for check resource removal keywoard.
TM-158: Fix Loadbalancers on EKS
TM-139: Fix tags
TM-136: Route53 improvement in VPC
TM-33: Initial commit with test scenarios for VPC module
TM-104: Add possibility to use VPN Lambda function in version 0.25.0 which add option to rotate certs.
TM-92: Fix aws provider
TM-88: Add Version to Policy
TM-71: Multiple Subnets
TM-75: Fix VPN authorize
TM-33: add VPC modules


## 0.24.2 2020-08-10
Add possibility to use VPN Lambda function in version 0.25.0 which add option to rotate certs.

## 0.24.1 2020-07-21
Fix VPN no authorize

## 0.24.0 2020-07-20
Initial package with updated VPN

#### Added
- Initial package for Lambda
